#pragma once

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<stdio.h>
#include<stdlib.h>
#include"synth.h"

#define WINDOW_WIDTH 624
#define WINDOW_HEIGHT 335
#define PIANO_X 18
#define PIANO_Y 300
#define PIANO_W 84
#define PIANO_H 32
#define PIANO_LEN 7  // How many octaves to draw

typedef struct Ui
{
	SDL_Renderer*r;
	SDL_Surface*surf_bg;
	SDL_Surface*surf_font;
	SDL_Surface*surf_knob[16];
	SDL_Surface*surf_piano12;
	SDL_Texture*tex_bg;
	SDL_Texture*tex_font;
	SDL_Texture*tex_knob[16];
	SDL_Texture*tex_piano12;
	SDL_Window*w;
	Synth*synth;
	bool running;
	bool shift_key;
	int32_t mouse_x[2];
	int32_t mouse_y[2];
	void*focused_knob;
	char**image_names;
} Ui;

void ui_close(Ui*ui);
void ui_init(Ui*ui,Synth*s);
void ui_update(Ui*ui);
