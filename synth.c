#include<stdbool.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>
#include"synth.h"

#ifndef M_PI
#define M_PI 3.14159
#endif

// Process channel buffer
void synth_process(Synth*synth)
{
	memset(synth->channel[synth->current_channel].buffer[0],0,SY_BUFFERSIZE*4);
}

void synth_create_channel(Synth*synth)
{
	synth->channel[synth->current_channel].freq=synth->knobs[K_FREQ].value;
	synth->channel[synth->current_channel].on=true;
	synth->channel[synth->current_channel].aenv_time=0;
	synth->channel[synth->current_channel].offset=0;
	synth->channel[synth->current_channel].playback_buffer=0;
}

/* void synth_next_buffer(Synth*synth) */
/* { */
/* } */

void synth_recalc(Synth*synth)
{
	const uint32_t oscillator_vol[]={K_OSC1_VOL,K_OSC2_VOL,K_OSC3_VOL,K_OSC4_VOL};
	const uint32_t oscillator_wavef[]={K_OSC1_WAVEF,K_OSC2_WAVEF,K_OSC3_WAVEF,K_OSC4_WAVEF};
	const uint32_t oscillator_duty[]={K_OSC1_PHASE,K_OSC2_PHASE,K_OSC3_PHASE,K_OSC4_PHASE};
	const uint32_t oscillator_det[]={K_OSC1_DET,K_OSC2_DET,K_OSC3_DET,K_OSC4_DET};

	// Clear buffer
	memset(synth->channel[synth->current_channel].buffer[0],0,SY_BUFFERSIZE*4);

	// Calculate lfo1 (master amp)
	if(synth->knobs[K_LFO1_VOL].value>0&&
			synth->knobs[K_LFO1_FREQ].value>0)
	{
		if(synth->knobs[K_LFO1_WAVEF].value<0.125)
		{
			synth->lfo1_offset=0;
			for(size_t i=0;i<SY_BUFFERSIZE;i+=SY_BSIZE)
			{
				size_t count=SY_BSIZE;
				if(i+count>=SY_BUFFERSIZE)
					count=SY_BUFFERSIZE-i;
				synth->lfo1_offset=gen_sin(synth->lfo1_buffer+i,
						synth->knobs[K_LFO1_VOL].value,
						synth->knobs[K_LFO1_FREQ].value,
						false,
						count,
						synth->samplerate,
						synth->knobs[K_LFO1_PHASE].value,
						synth->lfo1_offset);
			}
		}

		else if(synth->knobs[K_LFO1_WAVEF].value<0.25)
		{
			synth->lfo1_offset=gen_tri(synth->lfo1_buffer,
					synth->knobs[K_LFO1_VOL].value,
					synth->knobs[K_LFO1_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->knobs[K_LFO1_PHASE].value,
					synth->lfo1_offset);
		}

		else if(synth->knobs[K_LFO1_WAVEF].value<0.5)
		{
			synth->lfo1_offset=gen_saw(synth->lfo1_buffer,
					synth->knobs[K_LFO1_VOL].value,
					synth->knobs[K_LFO1_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->knobs[K_LFO1_PHASE].value,
					synth->lfo1_offset);
		}

		else if(synth->knobs[K_LFO1_WAVEF].value<0.75)
		{
			synth->lfo1_offset=gen_pul(synth->lfo1_buffer,
					synth->knobs[K_LFO1_VOL].value,
					synth->knobs[K_LFO1_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->knobs[K_LFO1_PHASE].value,
					NULL,
					synth->lfo1_offset);
		}

		else if(synth->knobs[K_LFO1_WAVEF].value<=1.0)
		{
			synth->lfo1_offset=gen_nse(synth->lfo1_buffer,
					synth->knobs[K_LFO1_VOL].value,
					synth->knobs[K_LFO1_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->lfo1_offset);
		}
	}

	// Calculate lfo2 (duty cycle)
	if(synth->knobs[K_LFO2_VOL].value>0&&
			synth->knobs[K_LFO2_FREQ].value>0)
	{
		if(synth->knobs[K_LFO2_WAVEF].value<0.125)
		{
			synth->lfo2_offset=0;
			for(size_t i=0;i<SY_BUFFERSIZE;i+=SY_BSIZE)
			{
				size_t count=SY_BSIZE;
				if(i+count>=SY_BUFFERSIZE)
					count=SY_BUFFERSIZE-i;
				synth->lfo2_offset=gen_sin(synth->lfo2_buffer+i,
						synth->knobs[K_LFO2_VOL].value,
						synth->knobs[K_LFO2_FREQ].value,
						false,
						count,
						synth->samplerate,
						synth->knobs[K_LFO2_PHASE].value,
						synth->lfo2_offset);
			}
		}

		else if(synth->knobs[K_LFO2_WAVEF].value<0.25)
		{
			synth->lfo2_offset=gen_tri(synth->lfo2_buffer,
					synth->knobs[K_LFO2_VOL].value,
					synth->knobs[K_LFO2_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->knobs[K_LFO2_PHASE].value,
					synth->lfo2_offset);
		}

		else if(synth->knobs[K_LFO2_WAVEF].value<0.5)
		{
			synth->lfo2_offset=gen_saw(synth->lfo2_buffer,
					synth->knobs[K_LFO2_VOL].value,
					synth->knobs[K_LFO2_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->knobs[K_LFO2_PHASE].value,
					synth->lfo2_offset);
		}

		else if(synth->knobs[K_LFO2_WAVEF].value<0.75)
		{
			synth->lfo2_offset=gen_pul(synth->lfo2_buffer,
					synth->knobs[K_LFO2_VOL].value,
					synth->knobs[K_LFO2_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->knobs[K_LFO2_PHASE].value,
					NULL,
					synth->lfo2_offset);
		}

		else if(synth->knobs[K_LFO2_WAVEF].value<=1.0)
		{
			synth->lfo2_offset=gen_nse(synth->lfo2_buffer,
					synth->knobs[K_LFO2_VOL].value,
					synth->knobs[K_LFO2_FREQ].value,
					false,
					SY_BUFFERSIZE,
					synth->samplerate,
					synth->lfo2_offset);
		}
	}

	// Oscillators
	for(size_t cur_osc=0;cur_osc<4;++cur_osc)
	{

		if(synth->knobs[K_AMP].value>0&&synth->knobs[oscillator_vol[cur_osc]].value>0)
		{

			// Sine
			if(synth->knobs[oscillator_wavef[cur_osc]].value<0.125)
			{
				float amp=synth->knobs[oscillator_vol[cur_osc]].value;
				float fr=(synth->knobs[K_FREQ].value/synth->knobs[K_FREQ].maxval)*MAXFREQ;
				fr+=SEMITONE(fr,synth->knobs[K_DET].value);
				fr+=SEMITONE(fr,synth->knobs[oscillator_det[cur_osc]].value);
				float duty=synth->knobs[oscillator_duty[cur_osc]].value;
				bool mix=cur_osc>0&&synth->knobs[oscillator_vol[cur_osc-1]].value>0;
				if(fr==0.0)break;
				synth->channel[synth->current_channel].offset=0;
				for(size_t i=0;i<SY_BUFFERSIZE;i+=SY_BSIZE)
					synth->channel[synth->current_channel].offset=
						gen_sin(synth->channel[synth->current_channel].buffer[0]+i,amp,fr,mix,SY_BSIZE,synth->samplerate,duty,
								synth->channel[synth->current_channel].offset);
			}

			// Triangle
			else if(synth->knobs[oscillator_wavef[cur_osc]].value<0.25)
			{
				float amp=synth->knobs[oscillator_vol[cur_osc]].value;
				float fr=(synth->knobs[K_FREQ].value/synth->knobs[K_FREQ].maxval)*MAXFREQ;
				fr+=SEMITONE(fr,synth->knobs[K_DET].value);
				fr+=SEMITONE(fr,synth->knobs[oscillator_det[cur_osc]].value);
				float duty=synth->knobs[oscillator_duty[cur_osc]].value;
				bool mix=cur_osc>0&&synth->knobs[oscillator_vol[cur_osc-1]].value>0;
				if(fr==0.0)break;
				synth->channel[synth->current_channel].offset=
					gen_tri(synth->channel[synth->current_channel].buffer[0],amp,fr,mix,SY_BUFFERSIZE,synth->samplerate,duty,
							synth->channel[synth->current_channel].offset);
			}

			// Sawtooth
			else if(synth->knobs[oscillator_wavef[cur_osc]].value<0.5)
			{
				float amp=synth->knobs[oscillator_vol[cur_osc]].value;
				float fr=(synth->knobs[K_FREQ].value/synth->knobs[K_FREQ].maxval)*MAXFREQ/2.0;
				fr+=SEMITONE(fr,synth->knobs[K_DET].value);
				fr+=SEMITONE(fr,synth->knobs[oscillator_det[cur_osc]].value);
				float duty=synth->knobs[oscillator_duty[cur_osc]].value;
				bool mix=cur_osc>0&&synth->knobs[oscillator_vol[cur_osc-1]].value>0;
				if(fr==0.0)break;
				synth->channel[synth->current_channel].offset=
					gen_saw(synth->channel[synth->current_channel].buffer[0],amp,fr,mix,SY_BUFFERSIZE,synth->samplerate,duty,
							synth->channel[synth->current_channel].offset);
			}

			// Pulse
			else if(synth->knobs[oscillator_wavef[cur_osc]].value<0.75)
			{
				float amp=synth->knobs[oscillator_vol[cur_osc]].value;
				float fr=(synth->knobs[K_FREQ].value/synth->knobs[K_FREQ].maxval)*MAXFREQ;
				fr+=SEMITONE(fr,synth->knobs[K_DET].value);
				fr+=SEMITONE(fr,synth->knobs[oscillator_det[cur_osc]].value);
				bool mix=cur_osc>0&&synth->knobs[oscillator_vol[cur_osc-1]].value>0;
				float duty=synth->knobs[oscillator_duty[cur_osc]].value;
				float*duty_buf=NULL;
				if(synth->knobs[K_LFO2_VOL].value>0&&
						synth->knobs[K_LFO2_FREQ].value>0)
					duty_buf=synth->lfo2_buffer;
				if(fr==0.0)break;
				synth->channel[synth->current_channel].offset=
				gen_pul(synth->channel[synth->current_channel].buffer[0],amp,fr,mix,SY_BUFFERSIZE,synth->samplerate,duty,duty_buf,
						synth->channel[synth->current_channel].offset);
			}

			// Noise
			else if(synth->knobs[oscillator_wavef[cur_osc]].value<=1.0)
			{
				float amp=synth->knobs[oscillator_vol[cur_osc]].value;
				bool mix=cur_osc>0&&synth->knobs[oscillator_vol[cur_osc-1]].value>0;
				float fr=(synth->knobs[K_FREQ].value/synth->knobs[K_FREQ].maxval)*MAXFREQ;
				fr+=SEMITONE(fr,synth->knobs[K_DET].value);
				if(fr==0.0)break;
				synth->channel[synth->current_channel].offset=
					gen_nse(synth->channel[synth->current_channel].buffer[0],amp,fr,mix,SY_BUFFERSIZE,synth->samplerate,
							synth->channel[synth->current_channel].offset);
			}

		}
	}
	// End oscillators

	// Apply master volume
	for(size_t i=0;i<SY_BUFFERSIZE;++i)
	{
		float amp=synth->knobs[K_AMP].value;
		if(synth->knobs[K_LFO1_VOL].value>0)
		{
			amp+=synth->lfo1_buffer[i%SY_BUFFERSIZE];
			amp/=2.0;
		}
		synth->channel[synth->current_channel].buffer[0][i]*=amp;
	}

	// Convolution low-pass filter
	if(synth->knobs[K_AMP].value!=0)
	{
		if((size_t)synth->knobs[K_LP].value>0)
		{

			size_t attacklen=synth->knobs[K_FENV_ATK].value*SY_BUFFERSIZE;
			for(int32_t i=1;i<SY_BUFFERSIZE;++i)
			{
				float mult=((float)i)/attacklen;
				float sum=0;

				for(int32_t j=MAX(1,i-synth->knobs[K_LP].value);j<=i;++j)
					sum+=synth->channel[synth->current_channel].buffer[0][j];

				// Weight for filter wet/dry
				{
					float f_val=sum/(synth->knobs[K_LP].value+1);
					float o_val=synth->channel[synth->current_channel].buffer[0][i];
					float o_mult=MAX(0,1.0-mult);

					if(mult==0)
						synth->channel[synth->current_channel].buffer[0][i]=o_val;
					else if(mult==1||o_mult==0)
						synth->channel[synth->current_channel].buffer[0][i]=f_val;
					else
						synth->channel[synth->current_channel].buffer[0][i]=f_val*mult+o_val*MAX(0,1.0-mult);
				}
			}

		}
	}

	// High-pass filter
	if(synth->knobs[K_AMP].value!=0)
	{
		if((size_t)synth->knobs[K_HP].value>0)
		{

			for(int32_t i=1;i<SY_BUFFERSIZE;++i)
			{
				float sum=0;
				for(int32_t j=MAX(1,i-synth->knobs[K_HP].value);j<=i;++j)
					sum+=synth->channel[synth->current_channel].buffer[0][j];
				synth->channel[synth->current_channel].buffer[0][i]-=sum/(synth->knobs[K_HP].value+1);
			}

		}
	}

	// Amplitude envelope attack
	{
		size_t attacklen=synth->knobs[K_AENV_ATK].value*SY_BUFFERSIZE;
		for(size_t i=0;i<attacklen;++i)
		{
			float mult=((float)i)/attacklen;
			synth->channel[synth->current_channel].buffer[0][i]*=mult;
		}
	}

	// Amplitude envelope release
	{
		size_t releaselen=synth->knobs[K_AENV_REL].value*SY_BUFFERSIZE;
		for(size_t i=SY_BUFFERSIZE-releaselen;i<SY_BUFFERSIZE;++i)
		{
			float mult=((float)SY_BUFFERSIZE-i)/releaselen;
			synth->channel[synth->current_channel].buffer[0][i]*=mult;
		}
	}

	// Artificially clip at amplitude
	if(synth->knobs[K_LIM].value<1.0)
	{
		for(size_t i=0;i<SY_BUFFERSIZE;++i)
		{
			if(synth->channel[synth->current_channel].buffer[0][i]>synth->knobs[K_LIM].value)
				synth->channel[synth->current_channel].buffer[0][i]=synth->knobs[K_LIM].value;
			else if(synth->channel[synth->current_channel].buffer[0][i]<-synth->knobs[K_LIM].value)
				synth->channel[synth->current_channel].buffer[0][i]=-synth->knobs[K_LIM].value;
		}
	}

	// Fade out sound at end of buffer
	{
		size_t releaselen=SY_BUFFERSIZE-SY_BUFFERSIZE;
		for(size_t i=SY_BUFFERSIZE+1;i<SY_BUFFERSIZE;++i)
		{
			float mult=((float)SY_BUFFERSIZE-i)/releaselen;
			synth->channel[synth->current_channel].buffer[0][i]*=mult;
		}
	}

	synth->recalculate_waveform=false;
	synth->current_channel=(synth->current_channel+1)%synth->nchannels;
}

void synth_new(Synth*s)
{
	if(!s)return;

	memset(s,0,sizeof(Synth));
	s->octave=3;
	s->play_waveform=false;
	s->recalculate_waveform=true;
	s->total_knobs=0;
	s->samplerate=SY_SAMPLERATE;
	s->current_channel=0;
	s->wave=wav_create(s->samplerate,32,W_FMT_IEEE_FLOAT,1,SY_BUFFERSIZE*4);
	s->nchannels=SY_NCHANNELS;
}

size_t gen_sin(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		size_t offset
		)
{
	float sig=0;
	size_t cyclelen=samplerate/freq*2.0;
	size_t phase_offset=phase*cyclelen;
	size_t i=offset%(size_t)(cyclelen*2.0);
	for(size_t j=0;j<count;++j,++i)
	{
		sig=sin(M_PI/cyclelen*(i+phase_offset))*amplitude;
		if(!mix)buffer[j]=0;
		buffer[j]=(buffer[j]+sig)/(mix?2.0:1.0);
	}
	return i;
}

size_t gen_tri(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		size_t offset
		)
{
	float sig=0;
	float inc=(freq/(float)samplerate*amplitude);
	size_t cyclelen=samplerate/freq*4.0;
	size_t points[3]={0,cyclelen/4,cyclelen/4*3};
	size_t phase_offset=phase*cyclelen;
	size_t i=offset%(size_t)(cyclelen*2.0);

	for(;i<count;++i)
	{
		size_t pos=i%(cyclelen*samplerate);

		if(freq==0.0)break;

		if((pos+phase_offset)%cyclelen==points[0])
			sig=0.0;
		else if((pos+phase_offset)%cyclelen==points[1])
			sig=amplitude;
		else if((pos+phase_offset)%cyclelen<points[1])
			sig+=inc;
		else if((pos+phase_offset)%cyclelen==points[2])
			sig=-amplitude;
		else if((pos+phase_offset)%cyclelen<points[2])
			sig-=inc;
		else
			sig+=inc;

		if(!mix)buffer[i]=0;
		buffer[i]=(buffer[i]+sig)/(mix?2.0:1.0);
	}

	return i;
}

size_t gen_saw(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		size_t offset
		)
{
	float sig=0;
	float inc=(freq/(float)samplerate*amplitude);
	size_t cyclelen=samplerate/freq*4.0;
	size_t points[3]={0,cyclelen/4,cyclelen/4*3};
	size_t phase_offset=phase*cyclelen;
	size_t i=offset;

	for(;i<count;++i)
	{
		size_t pos=i%(cyclelen*samplerate);

		if(freq==0.0)break;

		if((pos+phase_offset)%cyclelen==points[0])
			sig=0.0;
		else if((pos+phase_offset)%cyclelen==points[1])
			sig=-amplitude;
		else if((pos+phase_offset)%cyclelen<points[1])
			sig+=inc;
		else if((pos+phase_offset)%cyclelen==points[2])
			sig=-amplitude;
		else if((pos+phase_offset)%cyclelen<points[2])
			sig+=inc;
		else
			sig+=inc;

		if(!mix)buffer[i]=0;
		buffer[i]=(buffer[i]+sig)/(mix?2.0:1.0);
	}

	return i;
}

size_t gen_pul(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		float*duty_buf,
		size_t offset
		)
{
	float sig=0;
	size_t i=offset;
	for(;i<count;++i)
	{
		size_t cyclelen=samplerate/freq*4.0;
		size_t pos=i%(cyclelen*samplerate);

		if(duty_buf)
		{
			phase=fabs(duty_buf[i%SY_BUFFERSIZE]);
			phase=MAX(phase,0.001);
			phase=MIN(phase,0.999);
		}

		if(phase==0)
			phase=0.01;
		if(phase==1)
			phase=0.999;
		size_t points[2]={0,cyclelen*phase};

		if(freq==0.0)break;

		if(pos%cyclelen==points[0])
			sig=-amplitude*(1.0-phase);
		else if(pos%cyclelen==points[1])
			sig=amplitude*phase;

		if(!mix)buffer[i]=0;
		buffer[i]=(buffer[i]+sig)/(mix?2.0:1.0);
	}

	return i;
}

size_t gen_nse(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		size_t offset
		)
{
	float sig=0;
	size_t i=offset;
	samplerate=samplerate; // Inhibit unused variable warning
	freq=freq; // Inhibit unused variable warning
	for(;i<count;++i)
	{
		sig=(fmod(frand(),1.0)-fmod(frand(),1.0))*amplitude;
		if(!mix)buffer[i]=0;
		buffer[i]=(buffer[i]+sig)/(mix?2.0:1.0);
	}

	return i;
}
