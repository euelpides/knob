#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<math.h>
#include<portaudio.h>
#include<stdbool.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include"wav.h"
#include"knob.h"
#include"synth.h"
#include"ui.h"

#ifdef __linux__
#include<unistd.h>
#elif defined(_WIN32)
#endif

void create_knobs(Synth*synth);
int play_stream(const void*input,void*output,
		unsigned long count,
		const PaStreamCallbackTimeInfo*ti,
		PaStreamCallbackFlags fl,void*userdata);

int main(void)
{

	PaStream*pa=NULL;
	Synth synth={0};
	Ui ui={0};

	// Init
	srand(time(NULL));
	synth_new(&synth);
	create_knobs(&synth);
	ui_init(&ui,&synth);
	Pa_Initialize();
	Pa_OpenDefaultStream(&pa,0,1,paFloat32,synth.samplerate,SY_BSIZE,play_stream,&synth);
	Pa_StartStream(pa);

	// Event loop
	while(ui.running)
		ui_update(&ui);

	// Close
	Pa_StopStream(pa);
	Pa_Terminate();
	ui_close(&ui);
	return 0;
}

void create_knobs(Synth*synth)
{
	const uint32_t cell_padx=16,cell_pady=22;
	const uint32_t cell_w=153,cell_h=75;
	const uint32_t knob_space=32;
	uint32_t x;
	uint32_t y;

#define create_osc(o,w,v,ph) do{\
	create_knob(synth->knobs+K_OSC##o##_VOL,"",x,y,v,1.0);++synth->total_knobs;\
	create_knob(synth->knobs+K_OSC##o##_WAVEF,"",x+=knob_space,y,0.0,1.0);++synth->total_knobs;\
	synth->knobs[K_OSC##o##_WAVEF].format=K_FMT_WAV;\
	create_knob(synth->knobs+K_OSC##o##_PHASE,"Phs",x+=knob_space,y,ph,1.0);++synth->total_knobs;\
	create_knob(synth->knobs+K_OSC##o##_DET,"Fine",x+=knob_space,y,0.0,1.0);++synth->total_knobs;\
	synth->knobs[K_OSC##o##_DET].maxval=2.0;\
	synth->knobs[K_OSC##o##_DET].minval=-2.0;\
}while(0)

#define create_lfo(o,w,v,ph) do{\
	create_knob(synth->knobs+K_LFO##o##_VOL,"",x,y,v,1.0);++synth->total_knobs;\
	create_knob(synth->knobs+K_LFO##o##_WAVEF,"",x+=knob_space,y,0.0,1.0);++synth->total_knobs;\
	synth->knobs[K_LFO##o##_WAVEF].format=K_FMT_WAV;\
	create_knob(synth->knobs+K_LFO##o##_PHASE,"Phs",x+=knob_space,y,ph,1.0);++synth->total_knobs;\
	create_knob(synth->knobs+K_LFO##o##_FREQ,"Freq",x+=knob_space,y,0.0,1.0);++synth->total_knobs;\
	synth->knobs[K_LFO##o##_FREQ].maxval=500.0;\
	synth->knobs[K_LFO##o##_FREQ].format=K_FMT_FREQ;\
}while(0)

	// Knob row 0
	/* x=cell_w*3+cell_padx; */
	y=cell_h*0+cell_pady;

	// Knob row 1
	x=0+cell_padx;
	y+=75;
	create_osc(1,0.0,0.7,0.5);

	x=cell_w+cell_padx;
	create_osc(2,0.0,0.0,0.5);

	x=cell_w*2+cell_padx;
	create_osc(3,0.0,0.0,0.5);

	x=cell_w*3+cell_padx;
	create_osc(4,0.0,0.0,0.5);

	// Knob row 2
	x=0+cell_padx;
	y+=75;
	create_knob(synth->knobs+K_AMP,"Amp",x,y,0.4,2.0);++synth->total_knobs;
	create_knob(synth->knobs+K_LIM,"Lim",x+=knob_space,y,1.0,1.0);++synth->total_knobs;
	create_knob(synth->knobs+K_DET,"Fine",x+=knob_space,y,0.0,2.0);++synth->total_knobs;
	synth->knobs[K_DET].minval=-2.0;
	create_knob(synth->knobs+K_FREQ,"",x+=knob_space,y,440.0,MAXFREQ);++synth->total_knobs;
	synth->knobs[K_FREQ].format=K_FMT_FREQ;

	x=cell_w*1+cell_padx;
	create_knob(synth->knobs+K_AENV_ATK,"A",x,y,0.0125,1.0);++synth->total_knobs;
	create_knob(synth->knobs+K_AENV_REL,"R",x+=knob_space,y,0.3,1.0);++synth->total_knobs;

	x=cell_w*2+cell_padx;
	create_knob(synth->knobs+K_LP,"LP",x,y,0.0,12.0);++synth->total_knobs;
	synth->knobs[K_LP].format=K_FMT_SC;
	create_knob(synth->knobs+K_HP,"HP",x+=knob_space*2,y,0.0,128.0);++synth->total_knobs;
	synth->knobs[K_HP].format=K_FMT_SC;

	x=cell_w*3+cell_padx;
	create_knob(synth->knobs+K_FENV_ATK,"A",x,y,0.0,1.0);++synth->total_knobs;
	create_knob(synth->knobs+K_FENV_REL,"R",x+=knob_space,y,0.0,1.0);++synth->total_knobs;

	// Knob row 3
	x=0+cell_padx;
	y+=75;
	create_lfo(1,0.0,0.0,0.5);

	x=cell_w*1+cell_padx;
	create_lfo(2,0.0,0.0,0.5);

	x=cell_w*3+cell_padx;
	create_knob(synth->knobs+K_OCT,"Oct",x,y,2.0,7.0);++synth->total_knobs;
	synth->knobs[K_OCT].format=K_FMT_DEC;
	create_knob(synth->knobs+K_NCH,"Nch",x+=knob_space,y,1.0,SY_NCHANNELS);++synth->total_knobs;
	synth->knobs[K_NCH].minval=1;
	synth->knobs[K_NCH].format=K_FMT_DEC;

#undef create_lfo
#undef create_osc
}

int play_stream(const void*input,void*output,
		unsigned long count,
		const PaStreamCallbackTimeInfo*ti,
		PaStreamCallbackFlags fl,void*userdata)
{
	float*in=(float*)input;
	float*out=(float*)output;
	Synth*synth=(Synth*)userdata;

	ti=ti; // Prevent unused-variable warnings
	fl=fl;
	in=in;

	if(synth->play_waveform)
	{
		if(synth->playback_pos<SY_BUFFERSIZE)
		{
			size_t n=MIN(synth->playback_pos+count,SY_BUFFERSIZE);
			size_t m=n-synth->playback_pos;
			/* memcpy(out,&synth->channel[synth->current_channel].buffer[0][synth->playback_pos],m*sizeof(float)); */

			// Mix all channels
			for(size_t i=0;i<m;++i)
			{
				out[i]=synth->channel[0].buffer[0][synth->playback_pos+i];
				for(size_t j=1;j<synth->nchannels;++j)
					out[i]+=synth->channel[j].buffer[0][synth->playback_pos+i];
				out[i]/=synth->nchannels;
			}
			synth->playback_pos=MIN(synth->playback_pos+count,SY_BUFFERSIZE);
		}
		if(synth->playback_pos>=SY_BUFFERSIZE)
		{
			synth->playback_pos=0;
			synth->play_waveform=false;
			synth->cleared=false;
		}
	}
	else
	{
		if(!synth->cleared)
		{
			memset(out,0,count*sizeof(float));
			synth->cleared=true;
		}
	}

	// Never stop the stream
	return 0;
}
