#pragma once
#include<stdbool.h>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"wav.h"
#include"knob.h"

#define MAX(x,y) ((x>y)?(x):(y))
#define MAXFREQ 20000.0
#define MIN(x,y) ((x<y)?(x):(y))
#define NOTE_C0 16.352
#define SEMITONE(b,n) ((b)*pow(1.05946309436,(n)))
#define frand() ((double)rand() / RAND_MAX)

#define SY_SAMPLERATE 44100
#define SY_BSIZE 512
#define SY_BUFFERSIZE (44100/3+5000)
#define SY_NCHANNELS 3
#define SY_NKNOBS    78

typedef struct Channel
{
	bool on;                         // Currently playing?
	float aenv_time;                 // Time in aenv_lvl
	float buffer[2][SY_BUFFERSIZE];  // Double-buffers
	float freq;                      // Frequency
	size_t aenv_lvl;                 // 0:A, 1:D, 2:S, 3:R
	size_t offset;                   // Offset for oscillators
	size_t playback_buffer;          // Rendered buffer
} Channel;

typedef struct Synth
{
	Channel channel[SY_NCHANNELS];
	Knob knobs[SY_NKNOBS];        // Knobs
	WAVE wave;                    // Header for WAV file
	bool cleared;                 // Buffer was cleared
	bool play_waveform;           // Are we currently rendering?
	bool recalculate_waveform;    // When to regenerate
	float lfo1_buffer[SY_BUFFERSIZE];
	float lfo2_buffer[SY_BUFFERSIZE];
	float octave;                 // Octave for keyboard
	size_t current_channel;       // Which channel to render to
	size_t nchannels;
	size_t playback_pos;          // Current position in playback
	size_t lfo1_offset;
	size_t lfo2_offset;
	size_t samplerate;            // Samplerate
	uint32_t total_knobs;         // How many knobs being rendered
} Synth;

typedef struct Note
{
	float freq;
} Note;

void synth_create_channel(Synth*synth);
void synth_new(Synth*s);
void synth_process(Synth*synth);
void synth_recalc(Synth*synth);

size_t gen_sin(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		size_t offset
		);

size_t gen_sin2(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		size_t offset
		);

size_t gen_tri(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		size_t offset
		);

size_t gen_saw(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		size_t offset
		);

size_t gen_pul(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		float phase,
		float*duty_buf,
		size_t offset
		);

size_t gen_nse(
		float*buffer,
		float amplitude,
		float freq,
		bool mix,
		size_t count,
		size_t samplerate,
		size_t offset
		);
