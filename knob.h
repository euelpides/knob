#pragma once

#include<stdio.h>
#include<stdint.h>
#include<string.h>

// Knob indexes
enum
{
	K_AMP, K_FREQ, K_DET, K_LIM,
	K_AENV_ATK, K_AENV_REL,
	K_OSC1_VOL, K_OSC1_WAVEF, K_OSC1_PHASE, K_OSC1_DET,
	K_OSC2_VOL, K_OSC2_WAVEF, K_OSC2_PHASE, K_OSC2_DET,
	K_OSC3_VOL, K_OSC3_WAVEF, K_OSC3_PHASE, K_OSC3_DET,
	K_OSC4_VOL, K_OSC4_WAVEF, K_OSC4_PHASE, K_OSC4_DET,
	K_HP, K_LP,
	K_FENV_ATK, K_FENV_REL,
	K_OCT, K_NCH,
	K_LFO1_VOL, K_LFO1_WAVEF, K_LFO1_PHASE, K_LFO1_FREQ,
	K_LFO2_VOL, K_LFO2_WAVEF, K_LFO2_PHASE, K_LFO2_FREQ,
};

// Knob value display formats
enum
{
	K_FMT_PERC,
	K_FMT_FLOAT,
	K_FMT_WAV,
	K_FMT_FREQ,
	K_FMT_HEX,
	K_FMT_DEC,
	K_FMT_SC,        // Represent samples/cycle as frequency
};

typedef struct Knob
{
	bool clicked;    // Is being manipulated
	char*title;      // Descriptive name
	float maxval;    // Maximum allowed value
	float minval;    // Minimum allowed value
	float value;     // Value
	float def_value; // Default value (for reset)
	int32_t h;       // Width
	int32_t w;       // Height
	int32_t x;       // Where to display
	int32_t y;       // Where to display
	uint32_t format; // Type of data
} Knob;

void create_knob(Knob*kn,char*title,uint32_t x,uint32_t y,float value,float maxval);
