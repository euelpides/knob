#include<stdbool.h>
#include<stdint.h>
#include<stdio.h>
#include<string.h>
#include"knob.h"

void create_knob(Knob*kn,char*title,uint32_t x,uint32_t y,float value,float maxval)
{
	memset(kn,0,sizeof(Knob));
	kn->x=x;
	kn->y=y;
	kn->w=20;
	kn->h=20;
	kn->maxval=maxval;
	kn->minval=0.0;
	kn->value=value;
	kn->def_value=value;
	kn->title=title;
}
