#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<stdio.h>
#include<stdlib.h>
#include"ui.h"

static char*_image_names[]={ "data/k00.png", "data/k01.png", "data/k02.png", "data/k03.png", "data/k04.png", "data/k05.png", "data/k06.png", "data/k07.png", "data/k08.png", "data/k09.png", "data/k10.png", "data/k11.png", "data/k12.png", "data/k13.png", "data/k14.png", "data/k15.png", };

static void render_string(SDL_Renderer*r,SDL_Texture*tex,uint32_t x1,uint32_t y1,const char*str);
static void render_image(SDL_Renderer*r,SDL_Texture*tex,uint32_t x,uint32_t y,uint32_t w,uint32_t h);
static void draw_waveform(Ui*ui,uint32_t channel,uint32_t wf_x,uint32_t wf_y,uint32_t wf_w,uint32_t wf_h);
void draw_oscilloscope(Ui*ui,uint32_t channel,uint32_t wf_x,uint32_t wf_y,uint32_t wf_w,uint32_t wf_h);

void ui_init(Ui*ui,Synth*s)
{
	ui->image_names=_image_names;

	SDL_Init(SDL_INIT_EVERYTHING);
	IMG_Init(IMG_INIT_PNG);

	ui->w=SDL_CreateWindow("knob",SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			WINDOW_WIDTH,WINDOW_HEIGHT,0);
	ui->r=SDL_CreateRenderer(ui->w,-1,0);

	SDL_SetRenderDrawBlendMode(ui->r,SDL_BLENDMODE_BLEND);
	ui->surf_font=IMG_Load("data/font.png");
	ui->tex_font=SDL_CreateTextureFromSurface(ui->r,ui->surf_font);

	ui->surf_bg=IMG_Load("data/bg.png");
	ui->tex_bg=SDL_CreateTextureFromSurface(ui->r,ui->surf_bg);

	ui->surf_piano12=IMG_Load("data/piano-12.png");
	ui->tex_piano12=SDL_CreateTextureFromSurface(ui->r,ui->surf_piano12);

	for(size_t i=0;i<16;++i)
	{
		ui->surf_knob[i]=IMG_Load(ui->image_names[i]);
		ui->tex_knob[i]=SDL_CreateTextureFromSurface(ui->r,ui->surf_knob[i]);
	}

	SDL_SetRenderDrawColor(ui->r,0,0,0,0);
	SDL_RenderClear(ui->r);
	SDL_RenderPresent(ui->r);

	ui->running=true;
	ui->synth=s;

}

void ui_close(Ui*ui)
{
	for(size_t i=0;i<16;++i)
	{
		if(ui->tex_knob[i])
			SDL_DestroyTexture(ui->tex_knob[i]);
		if(ui->surf_knob[i])
			SDL_FreeSurface(ui->surf_knob[i]);
	}
	if(ui->w)
		SDL_DestroyWindow(ui->w);
	if(ui->tex_font)
		SDL_DestroyTexture(ui->tex_font);
	if(ui->surf_font)
		SDL_FreeSurface(ui->surf_font);
	if(ui->tex_bg)
		SDL_DestroyTexture(ui->tex_bg);
	if(ui->surf_bg)
		SDL_FreeSurface(ui->surf_bg);
	if(ui->tex_piano12)
		SDL_DestroyTexture(ui->tex_piano12);
	if(ui->surf_piano12)
		SDL_FreeSurface(ui->surf_piano12);
	IMG_Quit();
	SDL_Quit();
}

void ui_update(Ui*ui)
{
	SDL_Event e;
	if(SDL_PollEvent(&e))
	{
		switch(e.type)
		{

			case SDL_QUIT:
				ui->running=false;
				break;

#define play_note(f) do{ui->synth->knobs[K_FREQ].value=ui->synth->knobs[K_FREQ].maxval*(f/MAXFREQ);\
	/* ui->synth->knobs[K_DET].value=0.5;\ */\
	ui->synth->recalculate_waveform=true;\
	ui->synth->play_waveform=true;\
	ui->synth->playback_pos=0;\
	ui->synth->channel[ui->synth->current_channel].offset=0;\
	synth_create_channel(ui->synth);\
	}while(0)
#define oct(x) ((x)*(float)(pow(2,((int32_t)ui->synth->knobs[K_OCT].value-4))))

				// Mouse input
			case SDL_MOUSEBUTTONDOWN:
				{
					ui->mouse_x[1]=ui->mouse_x[0];
					ui->mouse_y[1]=ui->mouse_y[0];
					SDL_GetMouseState(&ui->mouse_x[0],&ui->mouse_y[0]);

					/* char*notes7[]={"c","d","e","f","g","a","b",}; */
					/* char*notes12[]={"c","c#","d","d#","e","f","f#","g","g#","a","a#","b",}; */

					// Piano keyboard press
					if(ui->mouse_x[0]>PIANO_X)
						if(ui->mouse_x[0]-PIANO_X<PIANO_W*PIANO_LEN)
						{

							if(ui->mouse_y[0]>=PIANO_Y&&ui->mouse_y[0]<PIANO_Y+PIANO_H/2)
							{
								float note=(ui->mouse_x[0]-PIANO_X)/(PIANO_W/12.0);
								float note_freq=SEMITONE(NOTE_C0,((uint32_t)note));
								/* printf("%s\n",notes12[(uint32_t)fmod(ui->mouse_x[0]-PIANO_X,PIANO_W/12.0)]); */
								play_note(note_freq);
							}

							else if(ui->mouse_y[0]>=PIANO_Y&&ui->mouse_y[0]>PIANO_Y+PIANO_H/2&&ui->mouse_y[0]<PIANO_Y+PIANO_H)
							{
								const float note_octave=(ui->mouse_x[0]-PIANO_X)/PIANO_W;
								const float note_x=(ui->mouse_x[0]-PIANO_X)%PIANO_W;
								const float note_width=PIANO_W/7.0;
								const uint32_t note_i=(uint32_t)(note_x/note_width);
								float note_freq;
								float note;
								if(note_i<=2)
									note=12.0*note_octave+note_i*2;
								else
									note=12.0*note_octave+note_i*2-1;
								note_freq=SEMITONE(NOTE_C0,note);
								play_note(note_freq);
							}
						}

					// Check ui->synth->knobs for mouse presses
					for(size_t i=0;i<ui->synth->total_knobs;++i)
					{
						if(ui->mouse_x[0]>=ui->synth->knobs[i].x&&(ui->mouse_x[0])<ui->synth->knobs[i].x+ui->synth->knobs[i].w&&
								ui->mouse_y[0]>=ui->synth->knobs[i].y&&ui->mouse_y[0]<ui->synth->knobs[i].y+ui->synth->knobs[i].h)
						{
							if(e.button.button==SDL_BUTTON_LEFT)
							{
								ui->synth->knobs[i].clicked=true;
								ui->focused_knob=&ui->synth->knobs[i];
							}
							else if(e.button.button==SDL_BUTTON_RIGHT)
							{
								ui->synth->knobs[i].value=ui->synth->knobs[i].def_value;
								ui->synth->recalculate_waveform=true;
							}
						}
					}

				}
				break;

			case SDL_MOUSEMOTION:

				if(ui->focused_knob!=NULL)
				{
					Knob*kn=(Knob*)ui->focused_knob;
					float rate=0.0125;

					rate=fabs(kn->minval-kn->maxval)/100.0;

					// Shift key for fine control
					if(ui->shift_key)
					{
						if(kn->format==K_FMT_FREQ)
							rate=1;
						else
							rate/=4;
					}

					ui->mouse_x[1]=ui->mouse_x[0];
					ui->mouse_y[1]=ui->mouse_y[0];
					SDL_GetMouseState(&ui->mouse_x[0],&ui->mouse_y[0]);

					rate*=fabs((float)ui->mouse_y[1]-(float)ui->mouse_y[0]);

					if(ui->mouse_y[0]<ui->mouse_y[1])
					{
						kn->value+=rate;
						if(kn->value>kn->maxval)
							kn->value=kn->maxval;
						ui->synth->recalculate_waveform=true;
					}

					else if(ui->mouse_y[0]>ui->mouse_y[1])
					{
						kn->value-=rate;
						if(kn->value<kn->minval)
							kn->value=kn->minval;
						ui->synth->recalculate_waveform=true;
					}

				}
				break;

			case SDL_MOUSEBUTTONUP:
				{

					if(ui->focused_knob)
					{
						for(size_t i=0;i<ui->synth->total_knobs;++i)
						{
							if(ui->focused_knob==&ui->synth->knobs[i])
								ui->synth->knobs[i].clicked=false;
							ui->focused_knob=NULL;
						}
					}
				}
				break;

				// Keyboard input
			case SDL_KEYDOWN:
				switch(e.key.keysym.sym)
				{

					case SDLK_ESCAPE:
						ui->running=false;
						break;

					case SDLK_LSHIFT:
					case SDLK_RSHIFT:
						ui->shift_key=true;
						break;

					case SDLK_F3:
						ui->synth->knobs[K_OCT].value=((uint32_t)ui->synth->knobs[K_OCT].value+1)%8;
						break;

						// First octave
					case SDLK_z:play_note(oct(261.63));break;
					case SDLK_s:play_note(oct(277));break;
					case SDLK_x:play_note(oct(293.66));break;
					case SDLK_d:play_note(oct(311));break;
					case SDLK_c:play_note(oct(329.63));break;
					case SDLK_v:play_note(oct(349.23));break;
					case SDLK_g:play_note(oct(370));break;
					case SDLK_b:play_note(oct(392));break;
					case SDLK_h:play_note(oct(415));break;
					case SDLK_n:play_note(oct(440));break;
					case SDLK_j:play_note(oct(466));break;
					case SDLK_m:play_note(oct(493));break;

								// Second octave
					case SDLK_q:play_note(oct(523.25));break;
					case SDLK_2:play_note(oct(554));break;
					case SDLK_w:play_note(oct(587.33));break;
					case SDLK_3:play_note(oct(622));break;
					case SDLK_e:play_note(oct(659.26));break;
					case SDLK_r:play_note(oct(698.46));break;
					case SDLK_5:play_note(oct(740));break;
					case SDLK_t:play_note(oct(783.99));break;
					case SDLK_6:play_note(oct(831));break;
					case SDLK_y:play_note(oct(880));break;
					case SDLK_7:play_note(oct(932));break;
					case SDLK_u:play_note(oct(987.77));break;
					case SDLK_i:play_note(oct(1046.5));break;

					case SDLK_F1:
								ui->synth->recalculate_waveform=true;
								break;

								// write wave file
					case SDLK_F2:
								{
									const char*fname="f.wav";
									FILE*f=fopen(fname,"wb");
									if(f)
									{
										fwrite(&ui->synth->wave,1,sizeof(WAVE),f);
										fwrite(ui->synth->channel[ui->synth->current_channel].buffer[0],1,SY_BUFFERSIZE*sizeof(float),f);
										fclose(f);
										printf("Wrote file '%s'\n",fname);
									}
								}
								break;

					case SDLK_SPACE:
								ui->synth->play_waveform=true;
								ui->synth->playback_pos=0;
								ui->synth->channel[ui->synth->current_channel].offset=0;
								break;

#undef oct
#undef play_note

				}
				break;

				// Key release
			case SDL_KEYUP:
				switch(e.key.keysym.sym)
				{

					case SDLK_LSHIFT:
					case SDLK_RSHIFT:
						if(ui->shift_key)
							ui->shift_key=false;
						break;

				}
				break;

		}
	}

	// Recalculate waveform
	if(ui->synth->recalculate_waveform)
		/* synth_process(ui->synth); */
		synth_recalc(ui->synth);

	ui->synth->nchannels=ui->synth->knobs[K_NCH].value;

	// Render
	SDL_SetRenderDrawColor(ui->r,0,0,0,0);
	SDL_RenderClear(ui->r);
	render_image(ui->r,ui->tex_bg,0,0,WINDOW_WIDTH,WINDOW_HEIGHT);

	// Draw waveform
	for(size_t i=0;i<ui->synth->nchannels;++i)
		draw_waveform(ui,i,
				16,
				3+(70/ui->synth->nchannels)*i,
				152-16,
				70/ui->synth->nchannels-1
				);

	// Draw realtime oscilloscope
	for(size_t i=0;i<ui->synth->nchannels;++i)
		draw_oscilloscope(ui,i,152*(i+1)+16,2,152-16,70);

	// Spectrum analyzer?
	/* { */
	/* 	float sum_lp=0; */
	/* 	float sum_hp=0; */
	/* 	SDL_SetRenderDrawColor(ui->r,64,64,255,0); */
	/* 	SDL_RenderDrawLine(ui->r,0,300,640,300); */
	/* 	for(size_t f=0+1;f<12-1;++f) */
	/* 	{ */
	/* 		int i=1; */
	/* 		size_t f_lp=f-1; */
	/* 		size_t f_hp=f+1; */
	/* 		for(int32_t j=MAX(1,i-f_lp);j<=i;++j) */
	/* 			sum_lp+=ui->synth->channel[synth->current_channel].buffer[0][j]; */
	/* 		sum_lp/=f_lp+1; */
	/* 		for(int32_t j=MAX(1,i-f_hp);j<=i;++j) */
	/* 			sum_hp+=ui->synth->channel[synth->current_channel].buffer[0][j]; */
	/* 		sum_hp/=f_hp+1; */
	/* 		sum_hp=sum_lp-f_hp; */
	/* 		SDL_RenderDrawLine(ui->r,f*10,300,f*10,300+sum_hp); */
	/* 	} */
	/* } */

	// Spectrum analyzer?
	/* { */
	/* 	SDL_SetRenderDrawColor(ui->r,64,64,255,0); */
	/* 	SDL_RenderDrawLine(ui->r,0,400,640,400); */
	/* 	for(size_t f=20;f>0;--f) */
	/* 	{ */
	/* 		float sum=0; */
	/* 		for(int32_t j=f;j>=0;--j) */
	/* 			sum+=ui->synth->channel[synth->current_channel].buffer[0][j]; */
	/* 		sum/=f+1; */
	/* 		SDL_RenderDrawLine(ui->r,f*10,400,f*10,400-sum*80); */
	/* 	} */
	/* } */

	// Draw ui->synth->knobs
	for(size_t i=0;i<ui->synth->total_knobs;++i)
	{
		int32_t pos=0;
		char str[1024]={0};
		uint32_t x1=ui->synth->knobs[i].x;
		uint32_t y1=ui->synth->knobs[i].y-18;
		uint32_t x2=ui->synth->knobs[i].x;
		uint32_t y2=ui->synth->knobs[i].y+32;

		/* pos=(int32_t)(ui->synth->knobs[i].value*16.0)-1; */
		pos=(int32_t)(ui->synth->knobs[i].value/ui->synth->knobs[i].maxval*16.0)-1;
		if(pos>14)pos=15;
		if(pos<1)pos=0;

		render_image(ui->r,ui->tex_knob[pos],ui->synth->knobs[i].x,ui->synth->knobs[i].y,20,20);
		render_string(ui->r,ui->tex_font,x1,y1,ui->synth->knobs[i].title);

		switch(ui->synth->knobs[i].format)
		{

			case K_FMT_SC:
				{
					float fr=0;
					if(ui->synth->knobs[i].value>0)
						fr=ui->synth->samplerate/ui->synth->knobs[i].value;
					sprintf(str,"%dHz",(int32_t)fr);
					render_string(ui->r,ui->tex_font,x2,y2,str);
				}
				break;

			case K_FMT_DEC:
				sprintf(str,"%d",(int32_t)ui->synth->knobs[i].value);
				render_string(ui->r,ui->tex_font,x2,y2,str);
				break;

			case K_FMT_PERC:
				sprintf(str,"%d",(int32_t)(ui->synth->knobs[i].value/ui->synth->knobs[i].maxval*100.0*ui->synth->knobs[i].maxval));
				render_string(ui->r,ui->tex_font,x2,y2,str);
				break;

			case K_FMT_HEX:
				sprintf(str,"%X",((uint32_t)(ui->synth->knobs[i].value/ui->synth->knobs[i].maxval*256*ui->synth->knobs[i].maxval))-1);
				render_string(ui->r,ui->tex_font,x2,y2,str);
				break;

			case K_FMT_FREQ:
				sprintf(str,"%0.0fHz",ui->synth->knobs[i].value);
				render_string(ui->r,ui->tex_font,x2,y2,str);
				break;

			case K_FMT_WAV:
				{
					const char*waveforms[]={"sin","tri","saw","pul","nse"};
					uint32_t waveform=0;
					if(ui->synth->knobs[i].value<0.125)
						waveform=0;
					else if(ui->synth->knobs[i].value<0.25)
						waveform=1;
					else if(ui->synth->knobs[i].value<0.5)
						waveform=2;
					else if(ui->synth->knobs[i].value<0.75)
						waveform=3;
					else
						waveform=4;
					sprintf(str,"%s",waveforms[waveform]);
					render_string(ui->r,ui->tex_font,x2,y2,str);
				}
				break;
		}
	}

	// Draw keyboard
	for(size_t i=0;i<PIANO_LEN;++i)
	render_image(ui->r,ui->tex_piano12,PIANO_X+PIANO_W*i,PIANO_Y,PIANO_W,PIANO_H);

	SDL_RenderPresent(ui->r);
	SDL_Delay(0.02);
}

void render_image(SDL_Renderer*r,SDL_Texture*tex,uint32_t x,uint32_t y,uint32_t w,uint32_t h)
{
	if(!r||!tex||!w||!h)return;
	SDL_Rect sr;
	SDL_Rect dr;
	sr.x=0;sr.y=0;sr.w=w;sr.h=h;
	dr.x=x;dr.y=y;dr.w=w;dr.h=h;
	SDL_RenderCopy(r,tex,&sr,&dr);
}

void render_string(SDL_Renderer*r,SDL_Texture*tex,uint32_t x1,uint32_t y1,const char*str)
{
	if(!r||!tex)return;
	SDL_Rect sr;
	SDL_Rect dr;
	uint32_t glyph_w=10;
	uint32_t glyph_h=16;

	for(size_t i=0;str[i];++i)
	{
		uint32_t x2=0;
		uint32_t y2=0;

		if(str[i]>='A'&&str[i]<='Z')
			x2=str[i]-'A';
		else if(str[i]>='a'&&str[i]<='z')
		{
			x2=str[i]-'a';
			y2=1;
		}
		else if(str[i]>='0'&&str[i]<='9')
		{
			x2=str[i]-'0';
			y2=2;
		}
		else if(str[i]=='-')
		{
			x2=12;
			y2=2;
		}
		else if(str[i]=='.')
		{
			x2=11;
			y2=2;
		}
		else if(str[i]==',')
		{
			x2=10;
			y2=2;
		}
		else if(str[i]=='%')
		{
			x2=13;
			y2=2;
		}
		else if(str[i]==':')
		{
			x2=14;
			y2=2;
		}
		else continue;
		x2*=glyph_w;
		y2*=glyph_h;
		dr.x=x1+i*glyph_w;dr.y=y1;dr.w=glyph_w;dr.h=glyph_h;
		sr.x=x2;sr.y=y2;sr.w=glyph_w;sr.h=glyph_h;
		SDL_RenderCopy(r,tex,&sr,&dr);
	}
}

void draw_waveform(Ui*ui,uint32_t channel,uint32_t wf_x,uint32_t wf_y,uint32_t wf_w,uint32_t wf_h)
{
	SDL_SetRenderDrawColor(ui->r,86,86,86,255);
	SDL_RenderDrawLine(ui->r, wf_x,wf_y+wf_h/2,wf_x+wf_w,wf_y+wf_h/2);

	SDL_SetRenderDrawColor(ui->r,255,0,0,255);
	for(size_t i=1;i<SY_BUFFERSIZE;++i)
	{
		float xratio=(float)wf_w/SY_BUFFERSIZE;
		float yratio=(float)wf_h/2.0;
		float y1=ui->synth->channel[channel].buffer[0][i-1];
		float y2=ui->synth->channel[channel].buffer[0][i];
		y1=(y1>1?1:(y1<-1?-1:y1));
		y2=(y2>1?1:(y2<-1?-1:y2));
		SDL_RenderDrawLine(ui->r,
				wf_x+xratio*(i-1),
				wf_y+wf_h/2+yratio*y1,
				wf_x+xratio*i,
				wf_y+wf_h/2+yratio*y2
				);
	}

	// Draw Current position indicator
	if(ui->synth->play_waveform)
	{
		SDL_SetRenderDrawColor(ui->r,150,150,255,128);
		SDL_Rect rect={
			(uint32_t)wf_x,
			(uint32_t)wf_y,
			(uint32_t)((float)ui->synth->playback_pos/SY_BUFFERSIZE*wf_w),
			(uint32_t)wf_h+1
		};
		SDL_RenderFillRect(ui->r,&rect);
	}
}

void draw_oscilloscope(Ui*ui,uint32_t channel,uint32_t wf_x,uint32_t wf_y,uint32_t wf_w,uint32_t wf_h)
{
	if(ui->synth->play_waveform)
	{
		SDL_SetRenderDrawColor(ui->r,255,0,0,255);
		for(size_t i=ui->synth->playback_pos;
				i<ui->synth->playback_pos+SY_BSIZE&&i<SY_BUFFERSIZE;
				++i
				)
		{
			float y1=MIN(ui->synth->channel[channel].buffer[0][i-1],1.0);
			float y2=MIN(ui->synth->channel[channel].buffer[0][i],1.0);
			SDL_RenderDrawLine(ui->r,
					wf_x+((float)(i-ui->synth->playback_pos)-1)/SY_BSIZE*wf_w,wf_y+MIN(MAX(wf_h/2-y1*wf_h,wf_y),wf_h),
					wf_x+((float)(i-ui->synth->playback_pos))/SY_BSIZE*wf_w,wf_y+MIN(MAX(wf_h/2-y2*wf_h,wf_y),wf_h)
					);
		}
	}
}
