CFLAGS=  -Wfatal-errors -Wall -Wextra $(shell pkgconf --cflags sdl2 SDL2_image SDL2_ttf portaudio-2.0)
LDFLAGS= $(shell pkgconf --libs sdl2 SDL2_image SDL2_ttf portaudio-2.0)
OBJS= knob

all: $(OBJS)
knob: main.o knob.o synth.o wav.o ui.o
	$(CC) $^ -o $@ $(LDFLAGS)
%.o: %.c %.h
	$(CC) -c $< $(CFLAGS)
clean:
	$(RM) *.o $(OBJS)
